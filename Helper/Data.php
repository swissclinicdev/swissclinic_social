<?php

namespace Swissclinic\Social\Helper;

class Data extends \Magento\Framework\App\Helper\AbstractHelper
{

    private $_socialConfigPaths = [
        "instagram" => "swissclinic_social/social_config/instagram_url",
        "facebook" => "swissclinic_social/social_config/facebook_url",
        "pinterest" => "swissclinic_social/social_config/pinterest_url",
        "youtube" => "swissclinic_social/social_config/youtube_url"
    ];

    public function __construct(
        \Magento\Framework\App\Helper\Context $context
    )
    {
        parent::__construct($context);
    }

    /**
     * @param null $storeId
     * @return mixed
     */
    public function getSocialUrls($storeId = null)
    {
        $socialUrls = array();

        foreach ($this->_socialConfigPaths as $key => $value) {
            $socialUrls[$key] = $this->scopeConfig->getValue(
                $value,
                \Magento\Store\Model\ScopeInterface::SCOPE_STORE,
                $storeId
            );
        }

        return $socialUrls;
    }
}