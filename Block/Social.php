<?php

namespace Swissclinic\Social\Block;

use Magento\Store\Model\ScopeInterface;

class Social extends \Magento\Framework\View\Element\Template
{
    private $_store;
    private $_information;

    protected $urls;
    protected $_scopeConfig;

    public function __construct(
        \Magento\Catalog\Block\Product\Context $context,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        array $data = [],
        \Swissclinic\Social\Helper\Data $shlp
    )
    {

        $this->_shlp = $shlp;
        $this->_store = $storeManager->getStore();

        parent::__construct(
            $context,
            $data
        );
    }

    public function getUrls()
    {

        $storeId = $this->_store->getId();


        $urls = $this->_shlp->getSocialUrls($storeId);

        return $urls;
    }
}

?>